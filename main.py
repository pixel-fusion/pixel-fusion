import cv2
from file_utils import list_files
from image_processing import (
    load_images,
    load_video_frames,
    align_images_multiprocessing,
    average_images
)


def main(start_dir, num_processes):
    image_paths = list_files(start_dir)
    images = load_images(image_paths)
    # images = load_video_frames(start_dir)
    print('images loaded!')
    aligned_images = align_images_multiprocessing(images, num_processes)
    print('images aligned!!')
    averaged_image = average_images(aligned_images)
    print('image averaged!!!')
    result_image = averaged_image

    cv2.imwrite('result_image_median.png', result_image)


if __name__ == '__main__':
    main('photos/', 3)
