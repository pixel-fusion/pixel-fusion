import cv2
import numpy as np
from multiprocessing import Pool
from skimage.registration import phase_cross_correlation


def load_images(image_paths):
    images = [cv2.imread(path) for path in image_paths]
    # gray_images = [cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) for img in images]
    return images


def load_video_frames(video_path):
    cap = cv2.VideoCapture(video_path)
    frames = []
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        frames.append(frame)
    cap.release()
    return frames


def align_image(args):
    ref_image, img = args
    shift, _, _ = phase_cross_correlation(ref_image, img)
    transformation = np.float32([[1, 0, -shift[1]], [0, 1, -shift[0]]])
    aligned_img = cv2.warpAffine(img, transformation, (img.shape[1], img.shape[0]))
    return aligned_img


def align_images_multiprocessing(images, num_processes=None):
    ref_image = images[0]
    aligned_images = [ref_image]

    with Pool(processes=num_processes) as pool:
        # Pass the reference image and each image in the list to the align_image function
        results = pool.map(align_image, [(ref_image, img) for img in images[1:]])

        # Add the results to the aligned_images list
        aligned_images.extend(results)

    return aligned_images


def average_images(images):
    images = [np.array(img, dtype=np.float32) for img in images]
    averaged_img = np.median(images, axis=0).astype(np.uint8)
    print(averaged_img[0])
    return averaged_img


def upscale_and_sharpen(image):
    pass


paths = ['./photos/DSC05596.png', './photos/DSC05647.png', './photos/DSC05697.png']
images = load_images(paths)
print(images)
aligned_images = align_images_multiprocessing(images, 6)
averaged_image = average_images(aligned_images)
cv2.imwrite('result_image_median.png', averaged_image)
