import os
import glob


def list_files(directory):
    file_paths = [f for f in glob.glob(os.path.join(directory, '*')) if os.path.isfile(f)]
    return file_paths
